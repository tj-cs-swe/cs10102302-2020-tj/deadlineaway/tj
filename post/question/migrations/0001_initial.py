# Generated by Django 2.2 on 2020-08-13 13:53
from django.conf import settings
from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):
    """class Migration定义了依赖和包含的操作"""
    initial = True
    # 依赖包含的model
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]
    # 操作包含的函数
    operations = [
        migrations.CreateModel(
            name='QuestionPost',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('forum', models.CharField(max_length=10)),
                ('title', models.CharField(max_length=100)),
                ('description', models.TextField()),
                ('created', models.DateTimeField(default=django.utils.timezone.now)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('-created',),
            },
        ),
    ]
