from django import template
register = template.Library()

@register.filter('list')
def do_list(value):
    return range(0, value)