from django import forms
from .models import QuestionPost


class QuestionPostForm(forms.ModelForm):
    """发布问题的表单类"""
    class Meta:
        # 指明数据模型来源
        model = QuestionPost
        # 定义表单包含的字段:问题属于的分区，标题，描述，标签
        fields = ('title', 'description', 'file', 'forum')
