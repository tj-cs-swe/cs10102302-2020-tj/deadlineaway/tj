from django.test import TestCase
import datetime
from django.utils import timezone
from django.urls import reverse
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from post.question.models import QuestionPost
from record.collect.models import CollectRecord
from record.like.models import LikeRecord
from time import sleep


class QuestionPostModelTests(TestCase):

    def test_was_created_recently_with_future_question(self):
        """若问题创建时间为为未来，则错误返回 FALSE"""
        # 测试author接口
        author = User(username='user', password='test_password')
        author.save()

        future_question = QuestionPost(
            author=author,
            column=None,
            title='test_title',
            description='test_question',
            created=timezone.now() + datetime.timedelta(days=30)
        )

        self.assertIs(future_question.was_created_recently(), False)
        pass

    def test_was_updated_recently_with_future_question(self):
        """若问题更新时间为为未来，则错误返回 FALSE"""
        # 测试author接口
        author = User(username='user1', password='test_password')
        author.save()

        future_question = QuestionPost(
            author=author,
            column=None,
            title='test_title1',
            description='test_question1',
            updated=timezone.now() + datetime.timedelta(days=30)
        )

        self.assertIs(future_question.was_updated_recently(), False)
        pass

    def test_was_created_recently_with_seconds_before_question(self):
        """问题创建时间为 1 分钟内，返回TRUE"""
        # 测试author接口
        author = User(username='user2', password='test_password')
        author.save()

        seconds_before_question = QuestionPost(
            author=author,
            column=None,
            title='test_title2',
            description='test_question2',
            created=timezone.now() - datetime.timedelta(seconds=45)
        )

        self.assertIs(seconds_before_question.was_created_recently(), True)
        pass

    def test_was_updated_recently_with_seconds_before_question(self):
        """问题更新时间为 1 分钟内，返回TRUE"""
        # 测试author接口
        author = User(username='user3', password='test_password')
        author.save()

        seconds_before_question = QuestionPost(
            author=author,
            column=None,
            title='test_title3',
            description='test_question3',
            updated=timezone.now() - datetime.timedelta(seconds=45)
        )

        self.assertIs(seconds_before_question.was_updated_recently(), True)
        pass

    def test_was_created_recently_with_days_before_question(self):
        """问题创建时间为若干天内，返回FALSE"""
        # 测试author接口
        author = User(username='user4', password='test_password')
        author.save()

        days_before_question = QuestionPost(
            author=author,
            column=None,
            title='test_title4',
            description='test_question4',
            created=timezone.now() - datetime.timedelta(days=30)
        )

        self.assertIs(days_before_question.was_created_recently(), False)
        pass

    def test_question_title(self):
        """检查问题标题"""
        # 测试author接口
        author = User(username='user5', password='test_password')
        author.save()
        title = 'test_title5'

        question = QuestionPost(
            author=author,
            column=None,
            title=title,
            description='test_question5',
        )
        question.save()

        self.assertIs(question.__str__(), title)
        pass

    def test_get_absolute_url(self):
        """检查url"""
        # 测试author接口
        author = User(username='user6', password='test_password')
        author.save()
        title = 'test_title6'

        question = QuestionPost(
            author=author,
            column=None,
            title=title,
            description='test_question6',
        )
        question.save()

        self.assertEqual(question.get_absolute_url(), reverse('question:question_detail', args=[question.id, ]))
        pass

    def test_question_is_collected(self):
        """刚创建一个问题，没有被收藏，返回False，若创建一条收藏记录后，则返回True"""
        # 测试author接口
        author = User(username='author', password='test_password')
        author.save()
        user = User(username='user7', password='test_password')
        user.save()
        title = 'test_title7'

        question = QuestionPost(
            author=author,
            column=None,
            title=title,
            description='test_question7',
        )

        question.save()
        self.assertIs(question.question_is_collected(user), False)

        record = CollectRecord(
            content_type=ContentType.objects.get_for_model(QuestionPost),
            object_id=question.id,
            user=user,
            collected_time=timezone.now()
        )

        record.save()
        sleep(0.5)
        self.assertIs(question.question_is_collected(user), True)
        pass

    def test_question_is_liked(self):
        """刚创建一个问题，没有被收藏，返回False，若创建一条收藏记录后，则返回True"""
        # 测试author接口
        author = User(username='author2', password='test_password')
        author.save()
        user = User(username='user8', password='test_password')
        user.save()
        title = 'test_title8'

        question = QuestionPost(
            author=author,
            column=None,
            title=title,
            description='test_question8',
        )

        question.save()
        self.assertIs(question.question_is_liked(user), False)

        record = LikeRecord(
            content_type=ContentType.objects.get_for_model(QuestionPost),
            object_id=question.id,
            user=user,
            liked_time=timezone.now()
        )

        record.save()
        sleep(0.5)
        self.assertIs(question.question_is_liked(user), True)
        pass

    pass


def test_question_detail():
    """问题创建后，进入详情页面，检查内容是否正确"""
    # 测试author接口
    author = User(username='user1', password='test_password')
    author.save()
    title = 'test_title1'

    question = QuestionPost(
        author=author,
        column=None,
        title=title,
        description='test_question1',
    )
    question.save()

    url = reverse('question:question_detail', args=(question.id,))
    pass


class QuestionPostViewTests(TestCase):

    def test_question_update_display(self):
        """更新问题内容，检查是否正确"""
        author = User(username='user2', password='test_password')
        author.save()
        title = 'test_title2'
        old_post_time = timezone.now()

        question = QuestionPost(
            author=author,
            column=None,
            title=title,
            description='old_test_question2',
            created=old_post_time
        )
        question.save()
        sleep(0.5)

        question.description = 'new_test_question2'
        question.created = timezone.now()
        question.save()
        question.refresh_from_db()
        self.assertTrue(question.created > old_post_time)
        pass

    def test_question_create(self):
        """登录用户创建自己的问题，否则提示失败"""
        # 测试author接口
        username = 'test_user'
        password = 'test_password'
        author = User.objects.create(username=username)
        author.set_password(password)
        author.save()
        login = self.client.login(username=username, password=password)
        self.assertTrue(login)

        url = reverse('question:question_create')
        response = self.client.post(url, {'column': 'none', 'title': 'test_title', 'description': 'test_question'},
                                    follow=True)
        self.assertEqual(response.status_code, 200)
        pass

    def test_login_then_update_myself_question(self):
        """用户只能更新自己的问题，否则提示失败"""
        # 测试author接口
        user1 = User.objects.create(username='testuser', is_superuser=True)
        user1.set_password('12345')
        user1.save()

        question = QuestionPost(
            author=user1,
            column=None,
            title='test_title',
            description='test_question',
        )
        question.save()

        login = self.client.login(username='testuser', password='12345')
        self.assertTrue(login)

        url = reverse('question:question_update', args=(question.id,))

        response = self.client.post(url, {'column': 'none', 'title': 'test_title1', 'description': 'test_question1'}, )
        self.assertEqual(response.status_code, 302)

        # 测试修改后 内容是否更新  结果是并没有修改成功
        question.refresh_from_db()

    def test_login_then_update_other_question(self):
        """用户更新别人的的问题，提示失败"""
        # 测试author接口
        user1 = User.objects.create(username='test_user1', is_superuser=True)
        user1.set_password('test_password')
        user1.save()
        user2 = User.objects.create(username='test_user2', is_superuser=True)
        user2.set_password('test_password')
        user2.save()

        question = QuestionPost(
            author=user1,
            column=None,
            title='test_title',
            description='test_question',
        )
        question.save()

        login = self.client.login(username='test_user2', password='test_password')
        self.assertTrue(login)

        url = reverse('question:question_update', args=(question.id,))

        # 测试修改后 内容是否更新  结果是并没有修改成功
        question.refresh_from_db()

    def test_increase_views_but_not_change_updated_field(self):
        """请求详情视图时，不改变 updated 字段"""
        # 测试author接口
        author = User(username='user3', password='test_password')
        author.save()

        question = QuestionPost(
            author=author,
            column=None,
            title='test_title3',
            description='test_question3',
        )
        question.save()

        sleep(0.5)

        url = reverse('question:question_detail', args=(question.id,))

        viewed_question = QuestionPost.objects.get(id=question.id)
        self.assertIs(viewed_question.updated - viewed_question.created < timezone.timedelta(seconds=0.1), True)
        pass

    pass
