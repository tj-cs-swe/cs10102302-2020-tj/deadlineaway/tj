from django.contrib import admin
from .models import QuestionPost, QuestionColumn

admin.site.register(QuestionPost)

# 注册问题板块
admin.site.register(QuestionColumn)
