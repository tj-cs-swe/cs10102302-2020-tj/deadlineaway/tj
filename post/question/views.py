from django.shortcuts import render, redirect
from .models import QuestionPost, QuestionColumn
from .forms import QuestionPostForm
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from reply.answer.models import Answer
import os
from django.http import HttpResponse, StreamingHttpResponse
from django.conf import settings
from django.utils.http import urlquote
from django.db.models import Q
from django.core.paginator import Paginator
import markdown
from django.utils.html import strip_tags


# 搜索问题
def search_question(request):
    # 判断是否进行了搜索

    search = request.GET.get('search')
    # 如果进行了搜索
    if search:
        questions = QuestionPost.objects.filter(
            # 匹配标题(不区分大小写)
            Q(title__icontains=search) |
            # 匹配问题描述(不区分大小写)
            Q(description__icontains=search)
        ).order_by('-updated')
        context = {'questions': questions, 'search_q': search}
    # 如果未进行搜索
    else:
        # 修改变量名称（articles -> article_list）
        question_list = QuestionPost.objects.all()
        # 每页显示 1 篇文章
        paginator = Paginator(question_list, 5)
        # 获取 url 中的页码
        page = request.GET.get('page')
        # 将导航对象相应的页码内容返回给 articles
        questions = paginator.get_page(page)
        context = {'questions': questions, 'search_q': search}
    return render(request, 'search/search_question.html', context)


# 热门问题
def hot_question_list(request):
    def sorts():
        sort = ['-total_views', '-updated']
        return sort

    # 取出所有问题
    questions = list(QuestionPost.objects.order_by(*sorts())[:5])

    # 将问题标题构成list
    hot_titles = [question.title for question in questions]
    # 如果list长度不足5，则用'...'补齐
    hot_titles += ['...' for i in range(5 - len(questions))]
    # 将问题id构成list
    hot_ids = [question.id for question in questions]
    # 如果list长度不足5，则用'...'补齐
    hot_ids += [0 for i in range(5 - len(questions))]
    # 将title list传递给模板（templates）
    context = {'questions': questions}
    # render函数：载入模板，并返回context对象
    return context


# 板块问题
def column_question_list():
    questions = QuestionPost.objects.all().order_by('column', '-updated')
    context = {'column_questions': questions}
    return context


# 问题详情
def question_detail(request, id):
    question = QuestionPost.objects.get(id=id)
    # 更新浏览量
    question.total_views += 1
    # 保存新的浏览量
    question.save(update_fields=['total_views'])
    question.description = strip_tags(question.description)
    question.description = markdown.markdown(question.description,
                                             extensions=[
                                                 # 包含 缩写、表格等常用扩展
                                                 'markdown.extensions.extra',
                                                 # 语法高亮扩展
                                                 'markdown.extensions.codehilite',
                                             ])
    answers = Answer.objects.filter(question=id)
    context = {'question': question, 'answers': answers}
    # download_file(request,id)
    return render(request, 'question/detail.html', context)


# 发布问题
@login_required(login_url='/userprofile/login/')
def question_create(request):
    # 判断该用户是否有相关权限
    myuser = User.objects.get(id=request.user.id)
    if not myuser.has_perm('question.add_questionpost'):
        return HttpResponse('抱歉，您的相关权限已被管理员封禁；如有疑问，请联系管理员！！！')
    # 判断用户是否提交数据
    if request.method == "POST":
        # 将提交的数据赋值到表单实例
        question_post_form = QuestionPostForm(request.POST, request.FILES)
        # 判断提交的数据是否满足要求
        if question_post_form.is_valid():
            # 保存数据，但暂时不提交到数据库
            new_question = question_post_form.save(commit=False)
            # 指定数据库中 当前的用户
            new_question.author = User.objects.get(id=request.user.id)
            # 所有问题都需要归属于一个板块
            new_question.column = QuestionColumn.objects.get(id=request.POST['column'])
            # 将文章保存至数据库
            new_question.save()
            # 保存 tags 的多对多关系
            question_post_form.save_m2m()
            # 完成后提示发表成功
            messages.success(request, "发表问题成功。")
            # 完成后返回到文章详情
            return redirect("question:question_detail", id=new_question.id)
        # 数据不合法 ，返回错误信息
        else:
            columns = QuestionColumn.objects.all()
            context = {'question_post_form': question_post_form, 'columns': columns}  # debug
            messages.error(request, "表单内容有误，请重新填写。")
            return render(request, "question/create.html", context)
    # 如果用户请求获取数据
    else:
        # 创建表单实例
        question_post_form = QuestionPostForm()
        # 赋值上下文
        context = {'question_post_form': question_post_form}
        # 返回模板
        return render(request, 'question/create.html', context)


# 更新问题
@login_required(login_url='/userprofile/login/')
def question_update(request, id):
    """
    更新文章视图
    通过POST提交表单，更新 title,body字段
    GET方法进入初始界面
    id: 问题id
    """
    # 判断该用户是否有相关权限
    myuser = User.objects.get(id=request.user.id)
    if not myuser.has_perm('question.change_questionpost'):
        return HttpResponse('抱歉，您的相关权限已被管理员封禁；如有疑问，请联系管理员！！！')
    # 获取需要修改的具体文章的对象
    question = QuestionPost.objects.get(id=id)
    # answers = Answer.objects.filter(question_id=id)
    if request.user != question.author:  # 4.25 17:21 增添 只能删除本人的问题
        messages.error(request, "抱歉，你无权修改这篇文章")
        return redirect(question)
    # 判断用户是否为 POST提交表单数据
    if request.method == "POST":
        # 将提交的数据保存在表单实例中
        question_post_form = QuestionPostForm(request.POST, request.FILES)

        # 判断提交的数据是否满足模型要求
        if question_post_form.is_valid():
            # 保存新鞋入的title,body
            question_cd = question_post_form.cleaned_data
            question.title = question_cd['title']
            question.description = question_cd['description']
            question.forum = question_cd['forum']

            if request.POST['column'] != 'none':
                question.column = QuestionColumn.objects.get(id=request.POST['column'])
            else:
                question.column = None

            if 'file' in request.FILES:
                print("有文件！")
                question.file = question_cd['file']
            # question.file = question_cd["file"]
            # question.file=rquestion_cd['file']
            question.save()
            # 调用get_absolute_url
            return redirect(question)
        else:
            columns = QuestionColumn.objects.all()
            context = {'question': question, 'columns': columns}
            messages.error(request, "表单内容有误，请重新填写。")
            return render(request, "question/update.html", context)
    # 如果用户get 请求获取数据
    else:
        # 创建表单实例
        question_post_form = QuestionPostForm()
        # 赋值上下文 将question对象也穿进去，方便提取旧的内容
        context = {'question': question, 'question_post_form': question_post_form}
        # 将响应返回到模板中渲染
        return render(request, 'question/update.html', context)


# 删除问题
@login_required(login_url='/userprofile/login/')
def question_delete(request, id):
    # 根据id获取需要删掉的问题
    question = QuestionPost.objects.get(id=id)
    # 只能删除本人的问题
    if request.user != question.author:
        messages.error(request, "抱歉，你无权删除这篇文章")
        return redirect(question)
    # 调用.delete()方法删除问题
    question.delete()
    # 完成后返回文章详情
    return redirect("/")


@login_required(login_url='/userprofile/login/')
def download_file(request, id):
    # print(request)
    # print(id)
    # return HttpResponse('sdf')
    # 判断该用户是否有相关权限
    myuser = User.objects.get(id=request.user.id)
    if not myuser.has_perm('question.view_questionpost'):
        return HttpResponse('抱歉，您的相关权限已被管理员封禁；如有疑问，请联系管理员！！！')
    question = QuestionPost.objects.get(id=id)
    #
    filename = question.file
    filepath = os.path.join(settings.MEDIA_ROOT, str(filename))

    the_file_name = str(filename).split("/")[-1]  # 显示在弹出对话框中的默认的下载文件名
    # print(the_file_name)
    # filename = '/usr/local/media/file/{}'.format(the_file_name)  # 要下载的文件路径
    fp = open(filepath, 'rb')
    # print("有有有")
    response = StreamingHttpResponse(fp)
    # print("可以看也")
    # response = StreamingHttpResponse(t)
    response['Content-Type'] = 'application/octet-stream'

    response['Content-Disposition'] = 'attachment;filename="{0}"'.format(urlquote(the_file_name.encode('utf8')))
    return response
