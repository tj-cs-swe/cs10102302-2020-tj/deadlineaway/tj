from django import forms
from .models import ArticlePost


# 发布文章的表单类
class ArticlePostForm(forms.ModelForm):
    """class ArticlePostForm定义了Meta，描述来源和字段"""
    class Meta:
        """class Meta定义了来源和字段"""
        # 指明数据模型来源
        model = ArticlePost
        # 定义表单包含的字段
        fields = ('forum', 'title', 'body', 'attachment')
        # 标签，标题，正文，附件
