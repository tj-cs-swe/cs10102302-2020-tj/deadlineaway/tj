from django.apps import AppConfig


class ArticleConfig(AppConfig):
    """class ArticleConfig定义了名字"""
    name = 'post.article'
