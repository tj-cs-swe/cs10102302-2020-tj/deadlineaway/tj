from django.urls import path
from . import views

app_name = 'article'

urlpatterns = [
    path('article-detail/<int:id>/', views.article_detail, name='article_detail'),
    path('article-create/', views.article_create, name='article_create'),
    path('article-update/<int:id>/', views.article_update, name='article_update'),
    path('article-delete/<int:id>/', views.article_delete, name='article_delete'),
    path('article-download/<int:id>', views.download_file, name='download_file'),
]
