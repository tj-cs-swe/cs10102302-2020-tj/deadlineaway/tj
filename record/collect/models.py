from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import User
from ..common.models import MarkRecord, adapt_initializer_keywords


class CollectRecord(MarkRecord):
    """收藏记录类"""

    def __init__(self, *args, **kwargs):
        """需要做一个适配再进行初始化"""
        adapt_initializer_keywords(kwargs=kwargs, keyword_map={'marked_time': 'collected_time'})
        super().__init__(*args, **kwargs)

    @property
    def collected_time(self):
        """收藏记录创建的时间"""
        return super().marked_time

    class Meta:
        ordering = ['-marked_time']
