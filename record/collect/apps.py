from django.apps import AppConfig


class CollectConfig(AppConfig):
    name = 'record.collect'
