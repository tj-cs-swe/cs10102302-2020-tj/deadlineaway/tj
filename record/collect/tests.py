from django.urls import reverse
from django.utils import timezone

from ..common.abstract_tests import MarkViewTests
from ..common.tests import MarkModelTests
from .models import CollectRecord


class CollectModelTests(MarkModelTests):
    """收藏模型的测试"""

    def setup(self):
        super().setUp(CollectRecord)

    def test_liked_time_is_correct(self):
        """测试适配接口里的`collected_time`是否正确"""
        collect_record = CollectRecord(marked_time=timezone.now())
        self.assertEqual(collect_record.marked_time, collect_record.collected_time)


class CollectViewTests(MarkViewTests):
    """收藏View的测试"""

    def test_post_without_login(self, _=None):
        """测试在没有登陆的情况下发送表单"""
        super().abstract_test_post_without_login(reverse('collect:collect_change'))

    def test_404(self, _=None):
        """测试 404 """
        super().abstract_test_404(reverse('collect:collect_change') + 'nonsense')
