from django.http import JsonResponse
from record.collect.models import CollectRecord
from ..common import views as common_views


def collect_change(request):
    """收藏变化了（点击或取消）"""
    permission_string = 'collect.change_collectcount'

    def success_response(collect_count, is_collected):
        return JsonResponse({
            'status': 'SUCCESS',
            'collect_num': collect_count,
            'is_collected': is_collected
        })

    return common_views.mark_change(
        request=request,
        mark_record_class=CollectRecord,
        permission_string=permission_string,
        success_response=success_response)
