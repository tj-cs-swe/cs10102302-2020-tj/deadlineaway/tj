from django.urls import reverse
from django.utils import timezone

from ..common.tests import MarkModelTests
from ..common.abstract_tests import MarkViewTests
from .models import LikeRecord


class LikeModelTests(MarkModelTests):
    """点赞模型的测试"""

    def setup(self):
        super().setUp(LikeRecord)

    def test_liked_time_is_correct(self):
        """测试适配接口里的`liked_time`是否正确"""
        like_record = LikeRecord(marked_time=timezone.now())
        self.assertEqual(like_record.marked_time, like_record.liked_time)


class LikeViewTests(MarkViewTests):
    """点赞View的测试"""

    def test_post_without_login(self, _=None):
        """测试在没有登陆的情况下发送表单"""
        super().abstract_test_post_without_login(reverse('like:like_change'))

    def test_404(self, _=None):
        """测试 404 """
        super().abstract_test_404(reverse('like:like_change') + 'nonsense')
