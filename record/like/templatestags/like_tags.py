"""有关点赞类的template_tags"""

from ..models import LikeRecord
from ...common import template_tag

register = template_tag.register


def template_args(instance, arg):
    return template_tag.template_args(instance, arg)


def template_method(instance, method):
    return template_tag.template_method(instance, method)


@register.simple_tag
def get_likes_num(obj):
    """得到点赞数量"""
    return template_tag.get_record_count(LikeRecord, obj)
