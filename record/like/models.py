from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import User
from ..common.models import MarkRecord, adapt_initializer_keywords


class LikeRecord(MarkRecord):
    """点赞记录类"""

    def __init__(self, *args, **kwargs):
        adapt_initializer_keywords(kwargs=kwargs, keyword_map={'marked_time': 'liked_time'})
        super().__init__(*args, **kwargs)

    @property
    def liked_time(self):
        """点赞的时间"""
        return super().marked_time

    class Meta:
        ordering = ['-marked_time']
