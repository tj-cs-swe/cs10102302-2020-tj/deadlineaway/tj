from django.http import JsonResponse
from record.like.models import LikeRecord
from ..common import views as common_views


def like_change(request):
    """点赞变化了（点击或取消）"""
    permission_string = 'like.change_likecount'

    def success_response(like_count, is_liked):
        return JsonResponse({
            'status': 'SUCCESS',
            'likes_num': like_count,
            'is_liked': is_liked
        })

    return common_views.mark_change(
        request=request,
        mark_record_class=LikeRecord,
        permission_string=permission_string,
        success_response=success_response)
