from django.contrib.auth.decorators import login_required
from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponse
from django.contrib.auth.models import User


@login_required(login_url='/userprofile/login/')
def mark_change(request, mark_record_class,
                permission_string,
                success_response,
                error_message='抱歉，您的相关权限已被管理员封禁；如有疑问，请联系管理员！！！'):
    """当某个记录变化了（如点赞记录、收藏记录）会调用这个函数"""
    # 判断该用户是否有相关权限
    if not User.objects.get(id=request.user.id).has_perm(permission_string):
        return HttpResponse(error_message)

    # 获取数据和对应的对象
    content_type = ContentType.objects.get(model=request.GET.get('content_type'))
    object_id = request.GET.get('object_id')
    user = request.user

    # 如果记录不存在，django会帮助我们创建，并对应地设置created的值
    record, created = mark_record_class.objects.get_or_create(
        content_type=content_type,
        object_id=object_id,
        user=user
    )

    # 如果用户项取消点赞，那么就删除这条记录
    if not created:
        record.delete()
    record_count = mark_record_class.record_count(content_type=content_type, object_id=object_id)
    return success_response(record_count, created)
