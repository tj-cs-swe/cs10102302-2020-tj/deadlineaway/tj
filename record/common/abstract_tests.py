from django.contrib.auth.models import User
from django.test import TestCase


class MarkViewTests(TestCase):
    """一个抽象的视图测试，需要子类继承才能完成真正的测试"""

    def setUp(self):
        """预设值用户名和密码供后续使用"""
        self.username = 'hello'
        self.password = 'world'

    def create_user(self):
        """创建新的临时用户并且保存到测试数据库"""
        user = User.objects.create(username=self.username, is_superuser=True)
        user.set_password(self.password)
        user.save()

    def test_login(self):
        """测试登录情况"""
        self.create_user()
        is_logged_in = self.client.login(username=self.username, password=self.password)
        self.assertTrue(is_logged_in)

    def abstract_test_post_without_login(self, url):
        """不登陆点赞会重定向，返回302"""
        response = self.client.post(url)
        self.assertEqual(302, response.status_code)

    def abstract_test_404(self, url):
        """错误的url会导致404"""
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)
