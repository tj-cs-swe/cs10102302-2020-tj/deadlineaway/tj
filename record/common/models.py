from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.auth.models import User


class MarkRecord(models.Model):
    """抽象的记录类，子类可以是点赞记录、收藏记录、喜欢记录等"""
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField(default=0)
    content_object = GenericForeignKey("content_type", "object_id")
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    marked_time = models.DateTimeField(auto_now=True)

    @classmethod
    def record_count(cls, content_type, object_id):
        """一个属于类自己的方法，从所有记录中筛选符合条件的记录数量"""
        records = cls.objects.all().filter(content_type=content_type, object_id=object_id)
        return len(records)

    class Meta:
        """代表是抽象类，django的要求写法"""
        abstract = True


def adapt_initializer_keywords(kwargs, keyword_map):
    """适配器函数，运用适配器设计模式确保接口不变，来方便合作"""
    for name_to_be_adapted, name_to_adapt in keyword_map.items():
        # 把待适配的属性值赋给接受适配的原属性，再把该属性弹出（删除）
        if name_to_adapt in kwargs:
            kwargs[name_to_be_adapted] = kwargs[name_to_adapt]
            kwargs.pop(name_to_adapt)
