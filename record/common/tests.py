from django.test import TestCase
from django.utils import timezone

from .models import MarkRecord


class MarkModelTests(TestCase):
    """抽象模型的整体测试，会被子类继承"""
    def setUp(self, mark_record_class=MarkRecord):
        """该方法用来预设置，供子类继承并制定最终的count和record类"""
        self.mark_record_class = mark_record_class

    def test_mark_count_is_initially_zero(self):
        """测试最开始mark_count是0"""
        if self.mark_record_class is not MarkRecord:
            self.assertEqual(self.mark_record_class.record_count(content_type=None, object_id=None), 0)

    def test_mark_record_time_is_None(self):
        """测试一开始mark_record_time是None"""
        mark_record = self.mark_record_class()
        self.assertEqual(mark_record.marked_time, None)

    def test_mark_record_time_is_correct(self):
        """测试mark_record_time被正确地设置"""
        mark_record = self.mark_record_class(marked_time=timezone.now())
        self.assertGreaterEqual(timezone.now(), mark_record.marked_time)
