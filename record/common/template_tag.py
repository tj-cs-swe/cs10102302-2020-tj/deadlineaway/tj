"""
会在前端使用的template_tags类

这里定义的函数是抽取出来的公共部分，包中其他模块会依赖于这些函数
collect、like里都有对应的template_tags会对其调用
"""

from django import template
from django.contrib.contenttypes.models import ContentType

register = template.Library()


@register.filter(name='add_arg')
def template_args(instance, arg):
    """stores the arguments in a separate instance attribute"""
    if not hasattr(instance, "_TemplateArgs"):
        setattr(instance, "_TemplateArgs", [])
    instance._TemplateArgs.append(arg)
    return instance


@register.filter(name='call')
def template_method(instance, method):
    """retrieves the arguments if any and calls the method"""
    method = getattr(instance, method)
    if hasattr(instance, "_TemplateArgs"):
        to_return = method(*instance._TemplateArgs)
        delattr(instance, '_TemplateArgs')
        return to_return
    return method()


# 得到收藏数量
@register.simple_tag
def get_record_count(record_type, obj):
    content_type = ContentType.objects.get_for_model(obj)
    return record_type.record_count(content_type=content_type, object_id=obj.id)
