from django.db import models
from django.contrib.auth.models import User


class Profile(models.Model):
    """
    用户信息类
    每个用户都与django自带的基本用户信息有级联关系
    """
    # 与 User 模型构成一对一的关系
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')
    # 电话号码字段
    phone = models.CharField(max_length=20, blank=True)
    # 头像
    avatar = models.ImageField(upload_to='avatar/%Y%m%d/', blank=True)
    # 个人简介
    bio = models.TextField(max_length=500, blank=True)
    # 邮箱
    email = models.CharField(max_length=20, blank=True)

    def __str__(self):
        return 'user {}'.format(self.user.username)
