from django.urls import path
from . import views

app_name = 'userprofile'

urlpatterns = [
    # 用户登入
    path('login/', views.UserLogin.as_view(), name='login'),
    # 用户退出
    path('logout/', views.user_logout, name='logout'),
    # 用户退出
    path('register/', views.UserRegister.as_view(), name='register'),
    # 用户删除
    path('delete/<int:id>/', views.user_delete, name='delete'),
    # 用户安全删除
    path('safe-delete/<int:id>/', views.user_safe_delete, name='safe_delete'),
    # 用户查看信息
    path('profile/<int:id>/', views.profile, name='profile'),
    # 用户编辑信息
    path('profile-update/<int:id>/', views.ProfileUpdate.as_view(), name='profile_update'),
    # 用户修改密码
    path('password-update/<int:id>/', views.PasswordUpdate.as_view(), name='password_update'),
    # 用户查看点赞信息
    path('collect-record/<int:id>/', views.get_collect_record, name='get_collect_record'),
    # 用户封禁
    path('user-forbidden/<int:id>/', views.user_forbidden, name='user_forbidden'),
    # 查看用户发表的东西
    path('user-get_myob/<int:id>/', views.get_myob, name='user_get_myob'),
]
