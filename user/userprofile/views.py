from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.db.models import Q
from django.contrib.auth.hashers import check_password
from .models import Profile
from .forms import UserLoginForm, UserRegisterForm, UserProfileForm, UserPasswordForm
from record.collect.models import CollectRecord
from post.article.models import ArticlePost
from post.question.models import QuestionPost
# 引入分页模块
from django.core.paginator import Paginator
# 引入权限检查模块
from django.contrib.auth.models import User
from django.contrib.auth.models import Permission
from django.views.generic.base import TemplateView


class Content:
    """一般常量类"""

    def __init__(self):
        pass

    is_need_message = 1
    not_need_message = 2
    is_dangerous_message = 3
    is_login = 1
    is_register = 2
    login_error_message = "账号或密码输入不合法"
    danger_message = "请使用GET或POST请求数据"
    register_error_message = "注册表单有误，请重新输入~"
    update_error_message = "你没有权限修改此用户信息。"
    del_warn_message = "你没有删除操作的权限。"
    method_post = 'POST'
    method_get = 'GET'
    db_user_name = 'username'
    password_form = 'user_password_form'
    profile = 'profile'
    user = 'user'
    profile_form = 'profile_form'
    id = 'id'
    phone = 'phone'
    bio = 'bio'
    email = 'email'
    avatar = 'avatar'
    user_profile = "userprofile:profile"


class PasswordContent:
    """密码相关常量类"""

    def __init__(self):
        pass

    password_error_message = "原始密码输入错误！"
    password_different_message = "两次新密码输入不同！"
    password_success_message = "修改密码成功！"
    old_password = 'old_password'
    new_password = 'new_password'
    new_password_reconfirm = 'new_password2'
    db_user_password = 'password'


class PathContent:
    """路径相关常量类"""

    def __init__(self):
        pass

    login_page_path = 'user/login.html'
    register_page_path = 'user/register.html'
    search_user_page_path = 'search/search_user.html'
    setting_page_path = 'user/setting.html'
    my_ob_page_path = 'user/myob.html'
    detail_page_path = 'user/personaldetail.html'
    profile_page_path = 'user/profile.html'
    home_page_path = '/'
    login_url = '/userprofile/login/'


# 被前端调用 暂时不进行重构


def search_user(request):
    """根据前端获得的Search值对用户进行搜索"""
    # 判断是否进行了搜索
    search = request.GET.get('search')
    users = []
    # 如果进行了搜索
    if search:
        users = User.objects.filter(
            # 匹配用户名(不区分大小写)
            Q(username__icontains=search)
        )
    context = {'users': users, 'search_u': search}
    return render(request, PathContent.search_user_page_path, context)


def user_form(request, message_type, choose):
    """
    返回各种情况下需要返回的用户表单
    提取出的公共函数，方便其他函数进行返回
    """
    # 创建用户表单类
    if choose == Content.is_login:
        form = UserLoginForm()
    elif choose == Content.is_register:
        form = UserRegisterForm()
    else:
        form = UserLoginForm()
    # 赋予上下文
    context = {'form': form}
    if message_type == Content.is_need_message and choose == Content.is_login:
        messages.success(request, Content.login_error_message)
    elif message_type == Content.is_need_message and choose == Content.is_register:
        messages.success(request, Content.register_error_message)
    elif message_type == Content.is_dangerous_message:
        messages.success(request, Content.danger_message)
    else:
        pass
    if choose == Content.is_login:
        return render(request, PathContent.login_page_path, context)
    elif choose == Content.is_register:
        return render(request, PathContent.register_page_path, context)


# UserLogin Class


class UserLogin(TemplateView):
    """根据前端获得的用户登录表格信息，进行登录处理操作类（包含密码检查）"""
    template_name = PathContent.login_page_path

    def get(self, request, *args, **kwargs):
        return user_form(request, Content.not_need_message, Content.is_login)

    def post(self, request, *args, **kwargs):
        user_login_form = UserLoginForm(data=request.POST)
        # 判断数据是否有效
        if user_login_form.is_valid():
            # 清洗出合法数据
            data = user_login_form.cleaned_data
            # 检验账号、密码是否正确匹配数据库中的某个用户
            # 如果均匹配则返回这个 user 对象
            user = authenticate(username=data[Content.db_user_name], password=data[PasswordContent.db_user_password])
            if user:
                # 将用户数据保存在 session 中，即实现了登录动作
                login(request, user)
                # 返回主页链接
                return redirect(PathContent.home_page_path)
            else:
                return user_form(request, Content.is_need_message, Content.is_login)
        else:
            return user_form(request, Content.is_need_message, Content.is_login)


# 用户退出
def user_logout(request):
    """将已登录的用户执行登出操作"""
    # 用户登出
    logout(request)
    # 返回首页
    return redirect(PathContent.home_page_path)


def permissions():
    """赋予用户相关权限，方便创建用户，封禁用户时调用"""
    p1 = Permission.objects.get(codename='add_articlepost')
    p2 = Permission.objects.get(codename='change_articlepost')
    p3 = Permission.objects.get(codename='view_articlepost')
    p4 = Permission.objects.get(codename='add_questionpost')
    p5 = Permission.objects.get(codename='change_questionpost')
    p6 = Permission.objects.get(codename='view_questionpost')
    p7 = Permission.objects.get(codename='add_comment')
    p8 = Permission.objects.get(codename='change_comment')
    # 由于有重复字段，所以不能靠codename查询，用序号代替，所以如果新作数据迁移可能需要改这个地方
    # p9 = Permission.objects.get(codename = 'add_answer')
    # 序号重复，暂用其他权限名称
    # p9 = Permission.objects.get(id = 41)
    p9 = Permission.objects.get(codename='add_session')
    # p10 = Permission.objects.get(codename = 'change_answer')
    # p10 = Permission.objects.get(id = 42)
    p10 = Permission.objects.get(codename='change_session')
    p11 = Permission.objects.get(codename='change_collectcount')
    p12 = Permission.objects.get(codename='change_likecount')
    return [p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12]


class UserRegister(TemplateView):
    """根据前端获得的用户注册表格信息，进行注册录处理操作类（包含密码检查，用户权限授予"""

    def get(self, request, *args, **kwargs):
        return user_form(request, Content.not_need_message, Content.is_register)

    def post(self, request, *args, **kwargs):
        # 得到注册表单数据
        user_register_form = UserRegisterForm(data=request.POST)
        # 检测数据是否有效
        if user_register_form.is_valid():
            # 保存表单到暂存区
            new_user = user_register_form.save(commit=False)
            # 设置密码
            new_user.set_password(user_register_form.cleaned_data[PasswordContent.db_user_password])
            # 保存用户名密码到数据库
            new_user.save()
            # 保存好数据后立即登录
            login(request, new_user)
            # 赋予新用户权限
            new_user.user_permissions.set(permissions())
            # 返回主界面
            return redirect(PathContent.home_page_path)
        else:
            return user_form(request, Content.is_need_message, Content.is_register)


@login_required(login_url=PathContent.login_url)
def user_delete(request, id):
    """对已登录的用户进行删除操作"""
    # 得到用户id
    user = User.objects.get(id=id)
    # 验证登录用户、待删除用户是否相同
    if request.user == user:
        # 退出登录
        logout(request)
        # 删除数据
        user.delete()
        # 返回主界面
        return redirect(PathContent.home_page_path)
    else:
        return HttpResponse(Content.del_warn_message)


# 用户安全删除
@login_required(login_url=PathContent.login_url)
def user_safe_delete(request, id):
    # 判断是否是用户POST
    if request.method == Content.method_post:
        # 得到用户id
        user = User.objects.get(id=id)
        # 验证登录用户、待删除用户是否相同
        if request.user == user:
            # 退出登录
            logout(request)
            # 删除数据
            user.delete()
            # 返回主界面
            return redirect(PathContent.home_page_path)
        else:
            return HttpResponse(Content.del_warn_message)
    else:
        return HttpResponse(Content.danger_message)


@login_required(login_url=PathContent.login_url)
def profile(request, id):
    """查看用户个人信息界面，将个人信息返回至前端"""
    user = User.objects.get(id=id)
    if Profile.objects.filter(user_id=id).exists():
        profile = Profile.objects.get(user_id=id)
    else:
        profile = Profile.objects.create(user=user)

    context = {Content.profile: profile, Content.user: user}
    return render(request, PathContent.detail_page_path, context)


class ProfileUpdate(LoginRequiredMixin, TemplateView):
    """更新用户的个人信息类"""
    login_url = PathContent.login_url

    def get(self, request, *args, **kwargs):
        # class-based view html传递的参数是从kwargs的字典传递的
        id = kwargs[Content.id]
        user = User.objects.get(id=id)
        # user_id 是 OneToOneField 自动生成的字段
        if Profile.objects.filter(user_id=id).exists():
            profile = Profile.objects.get(user_id=id)
        else:
            profile = Profile.objects.create(user=user)
        profile_form = UserProfileForm()
        context = {Content.profile_form: profile_form, Content.profile: profile, Content.user: user}
        return render(request, PathContent.profile_page_path, context)

    def post(self, request, *args, **kwargs):
        id = kwargs[Content.id]
        print(id)
        user = User.objects.get(id=id)
        # user_id 是 OneToOneField 自动生成的字段
        if Profile.objects.filter(user_id=id).exists():
            profile = Profile.objects.get(user_id=id)
        else:
            profile = Profile.objects.create(user=user)
            # 验证修改数据者，是否为用户本人
        if request.user != user:
            return HttpResponse(Content.update_error_message)

        profile_form = UserProfileForm(request.POST, request.FILES)
        if profile_form.is_valid():
            # 取得清洗后的合法数据
            profile_cd = profile_form.cleaned_data
            profile.phone = profile_cd[Content.phone]
            profile.bio = profile_cd[Content.bio]
            profile.email = profile_cd[Content.email]
            user.email = profile_cd[Content.email]
            if Content.avatar in request.FILES:
                profile.avatar = profile_cd[Content.avatar]
            user.save()
            profile.save()
            # 带参数的 redirect()
            return redirect(Content.user_profile, id=id)
        else:
            return HttpResponse(Content.register_error_message)


class PasswordUpdate(LoginRequiredMixin, TemplateView):
    """更改用户密码类"""
    login_url = PathContent.login_url

    # get 方法

    def get(self, request, *args, **kwargs):
        user_password_form = UserPasswordForm()
        return render(request, PathContent.setting_page_path, {Content.password_form: user_password_form})

    # post 方法

    def post(self, request, *args, **kwargs):
        user = get_object_or_404(User, id=kwargs[Content.id])
        user_password_form = UserPasswordForm(request.POST)
        if user_password_form.is_valid():
            old_password = user_password_form.cleaned_data[PasswordContent.old_password]
            new_password = user_password_form.cleaned_data[PasswordContent.new_password]
            new_password_reconfirm = user_password_form.cleaned_data[PasswordContent.new_password_reconfirm]

            if not check_password(old_password, user.password):
                messages.error(request, PasswordContent.password_error_message)
                return render(request, PathContent.setting_page_path, {Content.password_form: user_password_form})

            if new_password != new_password_reconfirm:
                messages.error(request, PasswordContent.password_different_message)
                return render(request, PathContent.setting_page_path, {Content.password_form: user_password_form})

            user.set_password(new_password)
            user.save()
            logout(request)

            messages.success(request, PasswordContent.password_success_message)
            return redirect(PathContent.home_page_path)


@login_required(login_url=PathContent.login_url)
def get_collect_record(request, id):
    """用户查看其收藏的信息，根据情况作出相应的数据返回"""
    # 判断是否进行了搜索
    if request.user.id != id:
        return HttpResponse("你没有权限查看收藏信息。")
    search = request.GET.get('search')
    collect_records = CollectRecord.objects.filter(user=id)
    collections = []
    for collect_record in collect_records:
        # print(collect_record.content_type)
        # print(collect_record.object_id)
        try:
            object = collect_record.content_type.get_object_for_this_type(id=collect_record.object_id)
            if search:
                if collect_record.content_type.model == 'articlepost' and (
                        search in object.title or search in object.body):
                    collections.append({'content_type': collect_record.content_type.model, 'object': object})
                if collect_record.content_type.model == 'questionpost' and (
                        search in object.title or search in object.description):
                    collections.append({'content_type': collect_record.content_type.model, 'object': object})
                if collect_record.content_type.model == 'comment' and search in object.body:
                    collections.append({'content_type': collect_record.content_type.model, 'object': object})
                if collect_record.content_type.model == 'answer' and search in object.body:
                    collections.append({'content_type': collect_record.content_type.model, 'object': object})
            else:
                collections.append({'content_type': collect_record.content_type.model, 'object': object})
        except:
            print("query doesn't match error")

    length = len(collections)
    return render(request, 'user/collection.html', {'collections': collections, 'len': length, 'search': search})


@login_required(login_url=PathContent.login_url)
def user_forbidden(request, id):
    """管理员可以对用户的权限进行封禁"""
    now_user = User.objects.get(id=request.user.id)

    if not now_user.has_perm('userprofile.user_change'):
        return HttpResponse('抱歉，您的相关权限已被管理员封禁；如有疑问，请联系管理员！！！')
    # 取出对象用户
    my_user = User.objects.get(id=id)
    my_user.user_permissions.remove(permissions())
    messages.success(request, "封禁用户成功！")
    return redirect(PathContent.home_page_path)


def get_profile(request):
    """根据前端需求，返回当前登录用户的个人信息作为全局数据"""
    if request.user.is_authenticated:
        if Profile.objects.filter(user_id=request.user.id).exists():
            profile = Profile.objects.get(user_id=request.user.id)
        else:
            profile = Profile.objects.create(user=request.user)
        context = {'globle_profile': profile}
        return context
    return {'request1': request}


def get_myob(request, id):
    """用户查看自己发表的文章和问题，返回相关数据"""
    # 判断是否进行了搜索
    user = User.objects.get(id=id)
    questions = QuestionPost.objects.filter(author=id)
    articles = ArticlePost.objects.filter(author=id)
    column_content = []
    for question in questions:
        content = {'title': question.title, 'id': question.id, 'column': str(question.column),
                   'updated': question.updated, 'body': question.description, 'type': 'question'}
        column_content.append(content)
        pass
    for article in articles:
        content = {'title': article.title, 'id': article.id, 'column': str(article.column), 'updated': article.updated,
                   'body': article.body, 'type': 'article'}
        column_content.append(content)
        pass

    column_content = sorted(column_content, key=lambda x: (x['updated']), reverse=True)

    paginator = Paginator(column_content, 5)
    # 获取 url 中的页码
    page = request.GET.get('page')
    # 将导航对象相应的页码内容返回给 articles
    column_content = paginator.get_page(page)

    column_content = {'column_content': column_content, 'this_user': user}
    return render(request, PathContent.my_ob_page_path, column_content)
