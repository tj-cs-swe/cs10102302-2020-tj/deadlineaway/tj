from django.test import TestCase
from .models import Profile


class UserProfileModelTest(TestCase):
    def test_email_is_correct(self):
        """测试邮箱被正确初始化"""
        email = 'hello@hello.com'
        profile = Profile(email=email)
        self.assertEqual(profile.email, email)

    def test_bio_is_correct(self):
        """测试bio被正确初始化"""
        bio = '你好啊，我们是DeadlineAway'
        profile = Profile(bio=bio)
        self.assertEqual(profile.bio, bio)

    def test_phone_is_correct(self):
        """测试电话号码被正确初始化"""
        phone = '19929293939'
        profile = Profile(phone=phone)
        self.assertEqual(profile.phone, phone)

    def test_model_can_be_modified_correctly(self):
        """测试模型能被正确更改"""
        email = '1@1.com'
        bio = 'hello world'
        phone = '18819001900'
        profile = Profile()
        profile.email = email
        profile.bio = bio
        profile.phone = phone
        self.assertEqual(profile.email, email)
        self.assertEqual(profile.bio, bio)
        self.assertEqual(profile.phone, phone)
