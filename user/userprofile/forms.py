# 引入表单类
from django import forms
# 引入django自带的User数据模型
from django.contrib.auth.models import User
from .models import Profile


# 登录需要用到的表单
class UserLoginForm(forms.Form):
    """登录表单类"""
    username = forms.CharField()
    password = forms.CharField()


class UserRegisterForm(forms.ModelForm):
    """注册表单类"""
    # 复写 User 的密码
    password = forms.CharField()
    password2 = forms.CharField()

    # 定义数据，使用django的user类
    class Meta:
        model = User
        fields = ('username', 'email')

    # 对两次输入的密码是否一致进行检查
    def clean_password2(self):
        data = self.cleaned_data
        if data.get('password') == data.get('password2'):
            return data.get('password')
        else:
            raise forms.ValidationError("密码输入不一致,请重试。")


class UserProfileForm(forms.ModelForm):
    """用户信息表单类"""

    class Meta:
        model = Profile
        fields = ('phone', 'avatar', 'bio', 'email')


# 修改用户密码需要用到的表单
class UserPasswordForm(forms.Form):
    """修改用户密码表单类"""
    # 旧密码
    old_password = forms.CharField()
    # 新设置的密码
    new_password = forms.CharField()
    # 重复新密码
    new_password2 = forms.CharField()
