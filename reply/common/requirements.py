from django.contrib.auth.decorators import login_required as django_login_required


def login_required(function):
    """确保登陆，否则跳转到对应的url，通过@login_required调用该描述符"""
    return django_login_required(function=function, login_url='/userprofile/login/')
