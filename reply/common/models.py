from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericRelation
from django.db import models
from django.utils import timezone


class Reply:
    """回答模型，Answer 和 Comment的生成器类"""

    @staticmethod
    def post_content(post_content_type, related_name):
        """生成post_content字段"""
        return models.ForeignKey(
            post_content_type,
            on_delete=models.CASCADE,
            related_name=related_name
        )

    @staticmethod
    def user(related_name):
        """生成user字段"""
        return models.ForeignKey(
            User,
            on_delete=models.CASCADE,
            related_name=related_name
        )

    @staticmethod
    def created():
        """生成created字段"""
        return models.DateTimeField(default=timezone.now)

    @staticmethod
    def records(record_type, related_query_name):
        """生成records字段"""
        return GenericRelation(record_type, related_query_name=related_query_name)

    @staticmethod
    def is_recorded(records, user):
        """生成is_recorded字段"""
        filtered_records = records.filter(user=user)
        return len(filtered_records) != 0
