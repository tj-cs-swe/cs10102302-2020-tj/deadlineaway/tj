from django.apps import AppConfig


class AnswerConfig(AppConfig):
    name = 'reply.answer'
