from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django.urls import reverse
from post.question.models import QuestionPost
from record.like.models import LikeRecord
from record.collect.models import CollectRecord
from ..common.models import Reply


class Answer(models.Model):
    """问题的回答模型"""
    related_name = 'answers'
    question = Reply.post_content(QuestionPost, related_name)

    user = Reply.user(related_name)

    body = models.TextField()

    created = Reply.created()

    file = models.FileField(upload_to='answer_file/%Y%m%d/', blank=True)

    like_records = Reply.records(LikeRecord, related_name)
    # 收藏记录
    collect_records = Reply.records(CollectRecord, related_query_name=related_name)

    def get_absolute_url(self):
        """获得回答详细页面url"""
        return reverse('question:question_detail', args=[self.question.id])

    def answer_is_liked(self, user):
        """回答是否点赞"""
        return Reply.is_recorded(self.like_records, user)

    def answer_is_collected(self, user):
        """回答是否收藏"""
        return Reply.is_recorded(self.collect_records, user)

    def was_created_recently(self):
        """回答创建时间是否错误"""
        diff = timezone.now() - self.created
        return diff.days == 0 and 0 <= diff.seconds < 60

    class Meta:
        ordering = ('created',)

    def __str__(self):
        return self.body[:20]
