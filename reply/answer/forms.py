from django import forms
from .models import Answer


class AnswerForm(forms.ModelForm):
    """回答的表单类型"""
    class Meta:
        model = Answer
        fields = ['body','file']