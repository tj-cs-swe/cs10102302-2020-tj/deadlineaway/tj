import os

import markdown
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.models import User
from django.http import HttpResponse, StreamingHttpResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.utils.html import strip_tags
from django.utils.http import urlquote
from notifications.signals import notify

from post.question.models import QuestionPost
from .forms import AnswerForm
from .models import Answer
from ..common.requirements import login_required


@login_required
def answer_create(request, question_id):
    """回答创建"""
    # 判断该用户是否有相关权限
    if not User.objects.get(id=request.user.id).has_perm('sessions.add_session'):
        return HttpResponse('抱歉，您的相关权限已被管理员封禁；如有疑问，请联系管理员！！！')
    question = get_object_or_404(QuestionPost, id=question_id)
    # 将markdown语法渲染成html样式
    question.description = markdown.markdown(
        question.description,
        extensions=[
            # 包含 缩写、表格等常用扩展
            'markdown.extensions.extra',
            # 语法高亮扩展
            'markdown.extensions.codehilite',
        ]
    )
    # 处理 POST 请求
    render_url = 'answer/create.html'
    if request.method == 'POST':
        answer_form = AnswerForm(request.POST, request.FILES)
        if answer_form.is_valid():
            new_answer = answer_form.save(commit=False)
            new_answer.question = question
            new_answer.user = request.user
            new_answer.save()
            notify.send(
                request.user,
                recipient=question.author,
                verb='回答了你',
                target=question,
                action_object=new_answer,
            )
            return redirect(question)
        else:
            context = {'question': question, 'answer_form': answer_form}
            messages.error(request, "表单内容有误，请重新填写。")
            return render(request, render_url, context)
    # 如果用户请求获取数据
    else:
        answer_form = AnswerForm()
        context = {'question': question, 'answer_form': answer_form}
        return render(request, render_url, context)


@login_required
def answer_update(request, question_id, answer_id):
    """更新用户回答"""
    # 判断该用户是否有相关权限
    myuser = User.objects.get(id=request.user.id)
    if not myuser.has_perm('sessions.change_session'):
        return HttpResponse('抱歉，您的相关权限已被管理员封禁；如有疑问，请联系管理员！！！')
    question = get_object_or_404(QuestionPost, id=question_id)
    question.description = strip_tags(question.description)
    question.description = markdown.markdown(
        question.description,
        extensions=[
            # 包含 缩写、表格等常用扩展
            'markdown.extensions.extra',
            # 语法高亮扩展
            'markdown.extensions.codehilite',
        ]
    )
    new_answer = Answer.objects.get(id=answer_id)

    if request.user != new_answer.user:
        messages.error(request, "抱歉，你无权修改该回答")
        return redirect(question)
    # 处理 POST 请求
    if request.method == 'POST':
        answer_form = AnswerForm(request.POST, request.FILES)

        if answer_form.is_valid():
            answer_cd = answer_form.cleaned_data
            new_answer.body = answer_cd['body']
            if 'file' in request.FILES:
                print("有文件！")
                new_answer.file = answer_cd['file']
            new_answer.save()
            return redirect(question)
        else:
            context = {'question': question, 'answer_form': answer_form, 'answer': new_answer}
            messages.error(request, "表单内容有误，请重新填写。")
            return render(request, "answer/update.html", context)
    # 如果用户请求获取数据
    else:
        answer_form = AnswerForm()
        context = {'question': question, 'answer_form': answer_form, 'answer': new_answer}
        return render(request, "answer/update.html", context)


@login_required
def answer_delete(request, question_id, answer_id):
    """删除某个回答"""
    question = get_object_or_404(QuestionPost, id=question_id)
    answer = Answer.objects.get(id=answer_id)

    if request.user != answer.user:
        messages.error(request, "抱歉，你无权删除该回答")
        return redirect(question)
    answer.delete()
    return redirect(question)


@login_required
def download_file(request, id):
    """下载附件"""
    answer = Answer.objects.get(id=id)
    filename = answer.file
    filepath = os.path.join(settings.MEDIA_ROOT, str(filename))

    the_file_name = str(filename).split("/")[-1]  # 显示在弹出对话框中的默认的下载文件名
    fp = open(filepath, 'rb')
    response = StreamingHttpResponse(fp)
    response['Content-Type'] = 'application/octet-stream'

    response['Content-Disposition'] = 'attachment;filename="{0}"'.format(urlquote(the_file_name.encode('utf8')))
    return response
