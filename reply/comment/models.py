from django.db import models
from django.contrib.auth.models import User
from mptt.models import MPTTModel, TreeForeignKey
from ckeditor.fields import RichTextField
from post.article.models import ArticlePost
from record.like.models import LikeRecord
from record.collect.models import CollectRecord
from ..common.models import Reply


class Comment(MPTTModel):
    """文章的评论模型类，是一个树形结构"""
    # mptt树形结构
    parent = TreeForeignKey(
        'self',
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        related_name='children'
    )
    # 记录二级评论回复给谁, str
    reply_to = models.ForeignKey(
        User,
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        related_name='replyers'
    )

    related_name = 'comments'

    article = Reply.post_content(ArticlePost, related_name)
    user = Reply.user(related_name)
    body = RichTextField()
    created = Reply.created()

    like_records = Reply.records(LikeRecord, related_name)
    collect_records = Reply.records(CollectRecord, related_name)

    def comment_is_liked(self, user):
        """返回评论是否被点赞"""
        return Reply.is_recorded(self.like_records, user)

    def comment_is_collected(self, user):
        """返回评论是否被收藏"""
        return Reply.is_recorded(self.collect_records, user)

    class MPTTMeta:
        order_insertion_by = ['created']

    def __str__(self):
        return self.body[:20]
