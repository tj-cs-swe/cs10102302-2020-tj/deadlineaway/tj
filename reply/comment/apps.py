from django.apps import AppConfig


class AnswerQuestionConfig(AppConfig):
    name = 'reply.comment'
