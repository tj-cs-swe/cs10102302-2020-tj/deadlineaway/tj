from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.test import TestCase

from post.article.models import ArticlePost
from record.collect.models import CollectRecord
from record.like.models import LikeRecord
from reply.comment.models import Comment


class CommentModelTest(TestCase):
    """评论模型的测试类"""

    def test_comment_is_not_liked(self):
        """刚创建一个问题，没有被收藏，返回False，若创建一条收藏记录后，则返回True"""
        user, article, comment = CommentModelTest.create_new_data()
        self.assertEqual(comment.comment_is_liked(user), False)

    def test_comment_is_liked(self):
        self.abstract_test_comment_is_recorded(LikeRecord, Comment.comment_is_liked)

    def test_comment_is_not_collected(self):
        """刚创建一个问题，没有被收藏，返回False，若创建一条收藏记录后，则返回True"""
        user, article, comment = CommentModelTest.create_new_data()
        self.assertEqual(comment.comment_is_collected(user), False)

    def test_comment_is_collected(self):
        """测试评论被收藏"""
        self.abstract_test_comment_is_recorded(CollectRecord, Comment.comment_is_collected)

    def abstract_test_comment_is_recorded(self, record_type, is_liked):
        """测试评论被记录"""
        user, article, comment = CommentModelTest.create_new_data()

        record = record_type(
            content_type=ContentType.objects.get_for_model(Comment),
            object_id=comment.id,
            user=user,
        )

        record.save()
        self.assertEqual(is_liked(comment, user), True)

    _user_id = 0

    @classmethod
    def create_new_user(cls):
        """创建新用户"""
        user = User(username=f'user-${cls._user_id}', password='password')
        user.save()
        cls._user_id += 1
        return user

    @classmethod
    def create_new_article(cls, user):
        """创建新的文章"""
        article = ArticlePost(author=user, title='你好世界')
        article.save()
        return article

    @classmethod
    def create_new_comment(cls, user, article):
        """创建新的评论"""
        comment = Comment(user=user, article=article)
        comment.save()
        return comment

    @classmethod
    def create_new_data(cls):
        """创建新数据"""
        user = cls.create_new_user()
        article = cls.create_new_article(user)
        comment = cls.create_new_comment(user, article)
        return user, article, comment
