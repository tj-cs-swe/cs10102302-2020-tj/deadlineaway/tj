from django import forms
from .models import Comment


class CommentForm(forms.ModelForm):
    """评论的表单类型"""
    class Meta:
        model = Comment
        fields = ['body']
