"""coding=utf-8"""
from django.urls import path
from . import views

# 正在部署的应用的名称
app_name = 'blocks'

urlpatterns = [
    path('blocks-list/<int:targetnum>', views.blocks_list, name='blocks_list'),
]
