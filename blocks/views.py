"""coding=utf-8"""
from django.db.models import Q
from django.shortcuts import render
from post.article.models import ArticlePost
from post.question.models import QuestionPost

def sort_column(column_questions, column_articles):
    """sort_column"""
    column_contents = []

    for question in column_questions:
        content = {
            'title': question.title,
            'id': question.id,
            'column': str(question.column),
            'updated': question.updated,
            'type': 'question'
        }
        column_contents.append(content)

    for article in column_articles:
        content = {
            'title': article.title,
            'id': article.id,
            'column': str(article.column),
            'updated': article.updated,
            'type': 'article'
        }
        column_contents.append(content)
        #pass

    column_contents = sorted(column_contents,
                             key=lambda x: (x['column'], x['updated']),
                             reverse=True)

    return column_contents


def column_content(questions, articles, column):
    """column_content"""
    column_questions = []
    column_articles = []
    for question in questions:
        if question.column and question.column.title == column:
            column_questions.append(question)
    for article in articles:
        if article.column and article.column.title == column:
            column_articles.append(article)
    return {'questions': column_questions, 'articles': column_articles}


 
def blocks_list(request,targetnum):
    """视图函数"""
    column = ''
    if targetnum == 3:
        column = '交友'
    elif targetnum == 2:
        column = '生活'
    elif targetnum == 1:
        column ='学习'
    search = request.GET.get('search')
    if search:
        questions = QuestionPost.objects.filter(
            # 匹配标题(不区分大小写)
            Q(title__icontains=search) |
            # 匹配问题描述(不区分大小写)
            Q(description__icontains=search)).order_by('-updated')
        articles = ArticlePost.objects.filter(
            # 匹配标题(不区分大小写)
            Q(title__icontains=search) |
            # 匹配问题描述(不区分大小写)
            Q(body__icontains=search)).order_by('-updated')
    else:
        questions = QuestionPost.objects.all()
        articles = ArticlePost.objects.all()

    content = column_content(questions, articles, column)
    context = {
        'questions': content['questions'],
        'articles': content['articles'],
        'search': search,
        'column':column
    }

    return render(request, 'blocks/list.html', context)
